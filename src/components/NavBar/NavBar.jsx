import React from 'react';
import './NavBar.scss';

const NavBar = () => {
    return (
        <header className="header">
            <div className="recruitmentContainer">
                <div className="elips-1"/>
                <h4 className="recruitmentHeader">Recruitment task</h4>
            </div>
        </header>
    );
};

export default NavBar;
